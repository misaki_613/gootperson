using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class badGenerator : MonoBehaviour
{
    public GameObject spikePrefab;
    float span = 1.0f;
    float delta = 0;

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(spikePrefab) as GameObject;
            int px = Random.Range(-6, 8);
            go.transform.position = new Vector3(20, px, 12);
        }
    }
}
