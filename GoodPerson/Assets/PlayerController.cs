using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        //左矢印が押されたとき
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            transform.Translate(-1, 0, 0);//左に1動かす
        }

        //右矢印が押されたとき
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            transform.Translate(1, 0, 0);//右に1動かす
        }

        //上矢印が押されたとき
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            transform.Translate(0, 1, 0);//上に1動かす
        }

        //下矢印が押されたとき
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            transform.Translate(0, -1, 0);//下に1動かす
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "good")　//もし当たったGOのタグがgoodだったら
        {
            GameObject.Find("Canvas").GetComponent<ScoreController>().AddScore();
        }

       
    }
}
