using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        this.player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        //フレームごとに等速で落下させる
        transform.Translate(-0.01f, 0, 0);

        //画面外にでたらオブジェクトを破棄する
        if(transform.position.x<-50.0f)
        {
            Destroy(gameObject);
        }

        //当たり判定
        Vector2 p1 = transform.position;　　　　　　　　　　　　//敵の中心座標
        Vector2 p2 = this.player.transform.position;　　　　　　//プレイヤーの中心座標
        Vector2 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.5f;   //敵の半径
        float r2 = 1.0f;　//プレイヤーの半径

        if(d<r1+r2)
        {
            GameObject.Find("Canvas").GetComponent<ScoreController>().AddScore();

            //衝突した場合は敵を消す
            Destroy(gameObject);
        }
    }
}
