using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameDirector : MonoBehaviour
{
    Image hpGauge;


    // Start is called before the first frame update
    void Start()
    {
        this.hpGauge = GameObject.Find("hpGauge").GetComponent<Image>();
    }

    public void DecreaseHp()
    {
        this.hpGauge.fillAmount -= 0.1f;

    }

    void Update()
    {
        if (hpGauge.fillAmount <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
