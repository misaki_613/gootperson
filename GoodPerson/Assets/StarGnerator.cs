using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarGnerator : MonoBehaviour
{
    public GameObject starPrefab;
    float span = 1.5f;
    float delta = 0;

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(starPrefab) as GameObject;
            int px = Random.Range(-6, 10);
            int a = Random.Range(15, 20);
            go.transform.position = new Vector3(a, px, 12);
        }
    }
}
